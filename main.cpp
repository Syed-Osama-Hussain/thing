#include <iostream>
#include "thing.h"
using namespace std;


int main()
{
  Thing T(1,2,3);
  cout<<"T: ";
  T.show();
  Thing A=T;
  cout<<"A: ";
  A.show();
  Thing B;
  cout<<"B: ";
  B.show();
  B=T;
  cout<<"B: ";
  B.show();
  T.set(4,5,6);
  cout<<"T: ";
  T.show();
  cout<<"A: ";
  A.show();
  cout<<"B: ";
  B.show();
  system("pause");

  
}